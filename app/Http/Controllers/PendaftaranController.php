<?php

namespace App\Http\Controllers;

use App\Http\Requests\PendaftaranRequest;
use App\Models\Jurusan;
use App\Models\Pendaftaran;
use App\Services\PendaftaranService;
use Illuminate\Http\Request;

class PendaftaranController extends Controller
{

    private function getListJurusan()
    {
        $list_jurusan = [];
        foreach(Jurusan::all() as $value) {
            $list_jurusan[$value['id']] = $value['name'];
        }
        return $list_jurusan;
    }

    private function getListGender()
    {
        return [
            'L' => 'Laki-laki',
            'P' => 'Perempuan'
        ];
    }

    public function index() 
    {
        return view('pendaftaran.index');
    }

    public function json(Request $request, PendaftaranService $pendaftaranService)
    {
        $data = [];

        $no = intval($request['start']);
        $start = intval($request['start']);
        $length = intval($request['length']);
        $filter = "";

        if ( isset($request['start']) && $request['length'] != -1 ) {

            $start = intval($request['start']);
            $length = intval($request['length']);
        }

        if ( isset($request['search']) && $request['search']['value'] != '' ) {
            $filter = trim($request['search']['value']);
        }

        $daftar = $pendaftaranService->getAllDataPendaftaran($filter)
                            ->orderBy('id','desc')
                            ->skip($start)
                            ->take($length)
                            ->get();      
        
        $total_record_filter = $pendaftaranService->getAllDataPendaftaran()->count();

        $data = [];

        foreach ($daftar as $value) {
            ++$no;
            $value->no = $no;
            $value->jurusan = $value->jurusan->name;
            $value->edit = route('pendaftaran.edit', ['id' => $value->id]);
            $value->delete = route('pendaftaran.destroy', ['id' => $value->id]);
            $data[] = $value;    
        }

        die(json_encode([
            "draw"            => isset ( $request['draw'] ) ? intval( $request['draw'] ) : 0,
            "recordsTotal"    => intval( ( $total_record_filter ) ),
            "recordsFiltered" => intval( ( $total_record_filter ) ),
            'data' => $data
        ]));
    }

    public function create()
    {
        $jurusan = $this->getListJurusan();

        $gender = $this->getListGender();

        return view('pendaftaran.create', compact('jurusan', 'gender'));
    }

    public function store(PendaftaranRequest $request, PendaftaranService $pendaftaranService)
    {
        $create = $pendaftaranService->create($request->all());
        if ($create) {
            return redirect()->route('pendaftaran.list')->with(['success' => 'Pendaftaran Berhasil.']);
        } else { 
            return redirect()->route('pendaftaran.create')->withInput()->withErrors(['error' => 'Ada kesalahan. Silahkan coba beberapa saat lagi.']);
        }

    }

    public function edit($id)
    {
        $jurusan = $this->getListJurusan();

        $gender = $this->getListGender();

        $daftar = Pendaftaran::findOrfail($id);

        return view('pendaftaran.edit', compact('jurusan', 'gender', 'daftar'));
    }

    public function update(Request $request, $id)
    {
        $request->validate([
            'id'        => ['required'],
            'jurusan'   => ['required'],
            'name'      => ['required'],
            'gender'    => ['required'],
            'telp'      => ['required'],
            'email'     => ['required', 'email'],
            'address'   => ['required']
        ]);

        $update = Pendaftaran::where([
            'id'        => $request->id,
            'user_id'   => auth()->user()->id
        ])
        ->update([
            'user_id'       => auth()->user()->id,
            'jurusan_id'    => $request->jurusan,
            'name'          => $request->name,
            'gender'        => $request->gender,
            'telp'          => $request->telp,
            'email'         => $request->email,
            'address'       => $request->address
        ]);

        if ($update) {
            return redirect()->route('pendaftaran.list')->with(['success' => 'Ubah Pendaftaran Berhasil.']);
        } else { 
            return redirect()->route('pendaftaran.edit', ['id' => $id])->withInput()->withErrors(['error' => 'Ada kesalahan. Silahkan coba beberapa saat lagi.']);
        }
    }

    public function destroy($id)
    {
        $delete = Pendaftaran::findOrfail($id);
        $delete->delete();
        if($delete) {
            return response()->json(['response' => 'Pendaftaran Berhasil Dihapus'], 200);
        } else {
            return response()->json(['response' => 'Pendaftaran Gagal Dihapus'], 422);
        }
    }

    public function hitung()
    {
        return 3 + 5;
    }
}
