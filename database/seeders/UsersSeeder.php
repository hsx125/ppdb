<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Str;

class UsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {        
        User::create([
            'name'      => 'Ertha Dwi Setiyawan',
            'email'     => 'ertha.setiyawan@gmail.com',
            'password'  => bcrypt('tempe'),
            'role'      => 1
        ]);

        User::factory()->count(100)->create();
    }
}
