@extends('layout')
@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-lg-8">
            <h5>Pendaftaran</h5>
        </div>
        <div class="col-lg-4">
            <div class="pull-right mb-2">
                <a href="{{ route('pendaftaran.create') }}" class="btn btn-sm btn-primary">
                    Tambah
                </a>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            @if(session('success'))
            <div class="alert alert-success">
                {{ session('success') }}
            </div>
            @endif
            <div class="card">
                <div class="card-body">
                   
                    <div class="table-responsive">
                        <table class="table table-bordered mb-0">
                            <thead>
                                <tr>
                                    <th style="width: 2%;">No</th>
                                    <th style="width: 15%;">Nama</th>
                                    <th style="width: 8%;">Telp. (Mobile)</th>
                                    <th style="width: 10%;">Email</th>
                                    <th>Alamat</th>
                                    <th style="width: 7%;">Jenis Kelamin</th>
                                    <th style="width: 10%;">Jurusan</th>
                                    <th style="width: 7%;">Opsi</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@push('js')
<script>
function btnDelete(url) {
    var konfirm = confirm("Anda yakin akan menghapus data ini?")
    if (konfirm) {
        $.ajax({
            url: url,
            type: 'delete',
            dataType: 'json',
            success:function(res) {
                window.location.reload()
            },
            error: function(error) {
                alert(error)
            }
        })
    } else {
        return false
    }
}

var table = $('.table').DataTable({
    ordering: false,
    serverSide: true,
    processing: true,
    // fixedHeader: true,
    responsive: true,
    stateSave: true,
    ajax: {
        url: "{{ route('pendaftaran.json') }}",
        type: 'post',
        data: {
            status: $('#status').val()
        }
    },
    columns: [
        { "data": "no", sClass: "text-center" },
        { "data": "name" },
        { "data": "telp", sClass: "text-center" },
        { "data": "email" },
        { "data": "address" },
        { "data": "gender", sClass: "text-center" },
        { "data": "jurusan.name" },
        {
            "data": null,
            sClass: "text-center",
            render: function(data, type, row, meta) {
                return `
                        <a class="btn btn-sm btn-success" href="${data.edit}"><i class="fa fa-edit"></i></a>
                        <a class="btn btn-sm btn-danger" onclick="btnDelete('${data.delete}')"><i class="fa fa-trash"></i></a>
                    `
            }
        },
    ],
    initComplete: function() {
        var api = this.api();
        $("input[type='search']")
            .off(".DT")
            .on("keyup.DT, blur", function(e) {
                if (e.keyCode === 13) {
                    api.search(this.value).draw();
                }
            });
        $("input[type='search']").attr('placeholder', 'Press Enter to search')
    },
})


</script>
@endpush
