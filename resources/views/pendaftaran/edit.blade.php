@extends('layout')
@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-lg-12">
            <h5>Ubah Pendaftaran</h5>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-6">
            <div class="card">
                <div class="card-body">
                    @error('error')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                    @enderror
                    <form method="post" action="{{ route('pendaftaran.update', $daftar['id']) }}">
                        @csrf
                        @method('PUT')
                        <input type="hidden" name="id" value="{{ $daftar['id'] }}">
                        <div class="form-group">
                            {!! eform_select('Jurusan', 'jurusan', $jurusan, old('jurusan') ?? $daftar['jurusan_id']) !!}
                            @error('jurusan')
                            <div class="invalid-feedback" style="display: block;">
                                {{ $message }}
                            </div>
                            @enderror
                        </div>

                        <div class="form-group">
                            {!! eform_input('text', 'Nama Lengkap', 'name', old('name') ?? $daftar['name']) !!}
                            @error('name')
                            <div class="invalid-feedback" style="display: block;">
                                {{ $message }}
                            </div>
                            @enderror
                        </div>

                        <div class="form-group">
                            {!! eform_select('Jenis Kelamin', 'gender', $gender, old('gender') ?? $daftar['gender']) !!}
                            @error('gender')
                            <div class="invalid-feedback" style="display: block;">
                                {{ $message }}
                            </div>
                            @enderror
                        </div>

                        <div class="form-group">
                            {!! eform_input('text', 'Telp. (Mobile)', 'telp', old('telp') ?? $daftar['telp']) !!}
                            @error('telp')
                            <div class="invalid-feedback" style="display: block;">
                                {{ $message }}
                            </div>
                            @enderror
                        </div>

                        <div class="form-group">
                            {!! eform_input('text', 'Email', 'email', old('email') ?? $daftar['email']) !!}
                            @error('email')
                            <div class="invalid-feedback" style="display: block;">
                                {{ $message }}
                            </div>
                            @enderror
                        </div>

                        <div class="form-group">
                            {!! eform_input('text', 'Alamat', 'address', old('address') ?? $daftar['address'], '') !!}
                            @error('address')
                            <div class="invalid-feedback" style="display: block;">
                                {{ $message }}
                            </div>
                            @enderror
                        </div>

                        <div>
                            <a href="{{ route('pendaftaran.list') }}" class="btn btn-danger">Kembali</a>
                            <button type="submit" class="btn btn-primary">
                                Simpan
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@push('js')
<script>
$('form').on('submit', function(e) {
    $('a, button').addClass('disabled', true).attr('disabled', true)
})
</script>
@endpush
