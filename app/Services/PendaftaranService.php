<?php

namespace App\Services;

use App\Models\Pendaftaran;

class PendaftaranService
{
    public function getAllDataPendaftaran($filter = "")
    {
        $pendaftaran = false;
        if(auth()->user()->role == 1) {
            $pendaftaran = Pendaftaran::where(function($query) use ($filter) {
                            $query->where('name', 'like', '%' . $filter . '%')
                                    ->orWhere('email', 'like', '%' . $filter . '%')
                                    ->orWhere('telp', 'like', '%' . $filter . '%');
            });
        } else {
            $pendaftaran = Pendaftaran::where('user_id', auth()->user()->id)
                            ->where(function($query) use ($filter) {
                                $query->where('name', 'like', '%' . $filter . '%')
                                        ->orWhere('email', 'like', '%' . $filter . '%')
                                        ->orWhere('telp', 'like', '%' . $filter . '%');
                            });
        }
        
        return $pendaftaran;
    }

    public function create($request)
    {
        $request = (object) $request;

        return Pendaftaran::create([
            'user_id'       => auth()->user()->id,
            'jurusan_id'    => $request->jurusan,
            'name'          => $request->name,
            'gender'        => $request->gender,
            'telp'          => $request->telp,
            'email'         => $request->email,
            'address'       => $request->address
        ]);
    }
}